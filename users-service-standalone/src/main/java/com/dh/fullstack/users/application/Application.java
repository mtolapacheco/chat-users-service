package com.dh.fullstack.users.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

/**
 * @author marvin tola
 */
//para iniciar el proyecto
@Import(
        com.dh.fulstack.users.service.Config.class
)
@SpringBootApplication
public class Application {
    public static void main(String[] args){

        SpringApplication.run(Application.class, args);
    }
}
